# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit unpacker

DESCRIPTION="eID application for Slovak government identity authentication"
HOMEPAGE="https://eidas.minv.sk/download/Aplikacia_EID/"
SRC_URI="https://eidas.minv.sk/download/Aplikacia_EID/linux/ubuntu/Aplikacia_pre_eID_amd64_ubuntu.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND} sys-apps/pcsc-lite"
BDEPEND=""

S="."

src_unpack() {
	default
	unpack_deb ${WORKDIR}/Aplikacia_pre_eID_amd64_ubuntu.deb
}

src_install(){
	cp -R "${WORKDIR}/usr" "${D}" || die "install failed!"

	# TODO desktop file update?
}

pkg_postinst() {
	einfo "To make ${CATEGORY}/${PN} work properly remember to enable"
	einfo "sys-apps/pcsc-lite service to run. For example in systemd:"
	einfo ""
	einfo "systemctl enable pcscd.socket"
	einfo "systemctl start pcscd.socket"
}
