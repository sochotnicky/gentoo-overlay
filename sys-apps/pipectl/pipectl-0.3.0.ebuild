# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

DESCRIPTION="a simple named pipe management utility"
HOMEPAGE="https://github.com/Ferdi265/pipectl"


if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/Ferdi265/${PN}.git"
else
	SRC_URI="https://github.com/Ferdi265/pipectl/archive/refs/tags/v${PV}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

LICENSE="GPL-2"
SLOT="0"
IUSE=""

BDEPEND=">=dev-util/cmake-3.10"
RDEPEND=""
DEPEND="
	${RDEPEND}
"
