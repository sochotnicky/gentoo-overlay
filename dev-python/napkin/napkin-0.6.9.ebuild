# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{8..11} pypy3 )

inherit distutils-r1

DESCRIPTION="A tool to write sequence diagrams effectively as Python code."
HOMEPAGE="
https://github.com/pinetr2e/napkin
https://pypi.org/project/napkin/
"
SRC_URI="
	https://github.com/pinetr2e/${PN}/archive/refs/tags/v${PV}.tar.gz
		-> ${P}.gh.tar.gz
"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 hppa ~ia64 ~loong ~m68k ~mips ppc ppc64 ~riscv ~s390 sparc x86 ~x64-macos"
IUSE=""

distutils_enable_sphinx docs \
	'>=dev-python/docutils-0.14' \
	dev-python/pallets-sphinx-themes \
	dev-python/sphinxcontrib-log_cabinet \
	dev-python/sphinx-issues \
	dev-python/sphinx-tabs
distutils_enable_tests pytest

RDEPEND="dev-python/requests[$PYTHON_USEDEP]"

src_prepare() {
	# I don't care about tests and if I don't remove them install throws
	# errors
	rm -rf tests/ || die
	distutils-r1_src_prepare
}
