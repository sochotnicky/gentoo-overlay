# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit meson xdg-utils

if [[ ${PV} != *9999* ]]; then
	SRC_URI="https://codeberg.org/dnkl/fuzzel/archive/${PV}.tar.gz  -> ${P}.tar.gz"
	KEYWORDS="~amd64"
	S="${WORKDIR}/${PN}"
else
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/dnkl/fuzzel.git"
fi

DESCRIPTION="Application launcher for wlroots based Wayland compositors"
HOMEPAGE="https://codeberg.org/dnkl/fuzzel"
LICENSE="MIT"
SLOT="0"
# TODO add png/svg use flags? 
IUSE="+png +svg +cairo"

DEPEND="
	dev-libs/wayland
	>=media-libs/fcft-3.0.0
	media-libs/libpng
	gnome-base/librsvg
	x11-libs/cairo
	x11-libs/libxkbcommon
	x11-libs/pixman
"
RDEPEND="
	${DEPEND}
"
BDEPEND="
	app-text/scdoc
	dev-libs/tllist
	dev-libs/wayland-protocols
	png? ( media-libs/libpng )
	svg? ( gnome-base/librsvg )
"

src_configure() {
	local emesonargs=(
		$(meson_feature cairo enable-cairo)
	)
	meson_src_configure
}

src_install() {
	meson_src_install
}
