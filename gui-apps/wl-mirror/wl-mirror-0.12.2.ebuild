# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

DESCRIPTION="a simple Wayland output mirror client"
HOMEPAGE="https://github.com/Ferdi265/wl-mirror"


if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/Ferdi265/${PN}.git"
else
	SRC_URI="https://github.com/Ferdi265/wl-mirror/releases/download/v${PV}/${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

LICENSE="GPL-2"
SLOT="0"
IUSE="wl-present"

BDEPEND=">=dev-util/cmake-3.10"
RDEPEND="
	>=dev-libs/wayland-protocols-1.14
	wl-present? ( sys-apps/pipectl )
"
DEPEND="
	${RDEPEND}
"

src_install() {
	cmake_src_install

	if use wl-present; then
		dobin scripts/wl-present
	fi
}
