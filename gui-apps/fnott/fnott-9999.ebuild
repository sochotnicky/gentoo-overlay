# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit meson xdg-utils

if [[ ${PV} != *9999* ]]; then
	SRC_URI="https://codeberg.org/dnkl/fnott/archive/${PV}.tar.gz  -> ${P}.tar.gz"
	KEYWORDS="~amd64"
	S="${WORKDIR}/${PN}"
else
	inherit git-r3
	EGIT_REPO_URI="https://codeberg.org/dnkl/fnott.git"
fi

DESCRIPTION="Keyboard driven and lightweight Wayland notification daemon for
wlroots-based compositors."
HOMEPAGE="https://codeberg.org/dnkl/fnott"
LICENSE="MIT"
SLOT="0"
# TODO add png/svg use flags? 
IUSE=""

DEPEND="
	dev-libs/wayland
	media-libs/libpng
	gnome-base/librsvg
	x11-libs/cairo
	x11-libs/libxkbcommon
	x11-libs/pixman
	media-libs/fcft
"
RDEPEND="
	${DEPEND}
"
BDEPEND="
	app-text/scdoc
	dev-libs/tllist
	dev-libs/wayland-protocols
"
