# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

if [[ "${PV}" == 9999 ]]
then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/artizirk/wdisplays.git"
else
	SRC_URI="https://github.com/artizirk/wdisplays/archive/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi
inherit meson xdg

DESCRIPTION="GUI display configurator for wlroots compositors"
HOMEPAGE="https://github.com/artizirk/wdisplays"

BDEPEND="
	x11-libs/gtk+:3[wayland]
	gui-libs/wlroots"
DEPEND="${BDEPEND}"

LICENSE="GPL-3+"
SLOT="0"
