# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit acct-user

DESCRIPTION="Performance monitoring copilot user"
ACCT_USER_ID=898
ACCT_USER_GROUPS=( pcp )

acct-user_add_deps
